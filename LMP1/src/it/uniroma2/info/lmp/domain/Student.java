package it.uniroma2.info.lmp.domain;

public class Student extends Person {

    String matricola;
    public Student(String name, String surname, int birthYear, String codiceFiscale, String matricola) {
        super(name, surname, birthYear, codiceFiscale);
        this.matricola = matricola;
    }
}
