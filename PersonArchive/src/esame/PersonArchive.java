package esame;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;


public class PersonArchive {

    private Map<String, Person> lavoranti, nonLavoranti;

    public PersonArchive() {
        lavoranti = new HashMap<>();
        nonLavoranti = new HashMap<>();
    }

    //IMPIEGATO
    public Person creaPersona(String name, String surname, LocalDate birth, String codiceFiscale, String matricola, int livello, String mansione) throws PersonCreationException {
        Impiegato antonio = new Impiegato(name, surname, birth, codiceFiscale, matricola, livello, mansione);
        controlloAnn(antonio);
        return antonio;
    }

    //LIBERO PROF.
    public Person creaPersona(String name, String surname, LocalDate birth, String codiceFiscale, String professione, String partitaIVA) throws PersonCreationException {
        LiberoProfessionista antonio = new LiberoProfessionista(name, surname, birth, codiceFiscale, professione, partitaIVA);
        controlloAnn(antonio);
        return antonio;
    }

    //DI MAIO
    public Person creaPersona(String name, String surname, LocalDate birth, String codiceFiscale, String inscrizioneRegistroDisoccupazione) throws PersonCreationException {
        Disoccupato antonio = new Disoccupato(name, surname, birth, codiceFiscale, inscrizioneRegistroDisoccupazione);
        controlloAnn(antonio);
        return antonio;
    }

    public Person ricercaPersona(String codiceFiscale, boolean lavorante) {
        if (lavorante) {
            return lavoranti.get(codiceFiscale);
        }
        return nonLavoranti.get(codiceFiscale);
    }

    private void controlloAnn(Person p) {
        if (p.getClass().getAnnotation(StatusLavorativo.class).status().equals("Lavorante")) {
            lavoranti.put(p.getCodiceFiscale(), p);
        } else
            nonLavoranti.put(p.getCodiceFiscale(), p);
    }
}
