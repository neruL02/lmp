package it.uniroma2.esame.it.uniroma2.runner;

import it.uniroma2.esame.Attivita;
import it.uniroma2.esame.CardReader;

import java.io.IOException;

public class Runner {

  public static void main(String[] args) throws IOException {

    CardReader cr = new CardReader();
    Attivita negozio = cr.creaAttivita("negozioAntonio");
    System.out.println(negozio);
  }

}
