package it.uniroma2.info.lmp.domain;

public interface Person {
    int getBirthYear();
    String getName();

    String getSurname();

    String getCodiceFiscale();

    int getAge();
}
