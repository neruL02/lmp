package it.uniroma2.esame;

public class Ristorante extends AttivitaCommerciale{
  CategoriaRistorante categoria;

  public Ristorante(String sede, int inizioAttivita, String partitaIVA, CategoriaRistorante categoria) {

    super(sede, inizioAttivita, partitaIVA);
    this.categoria = categoria;
  }
}
