package esame;

public class Rivista extends Vendibile{

  public Rivista(String titolo, int quantita, int prezzoCopertina) {

    super(titolo, Genere.RIVISTA, quantita, prezzoCopertina);
  }
}
