package it.uniroma2.info.lmp.domain;

public class StudentImpl extends PersonImpl implements Student {

  private String studentId;

  private int courseYear;

  StudentImpl(String name, String surname, int birthYear, String codiceFiscale, String studentId, Course course,
              int courseYear) throws StudentCreationException, PersonCreationException {

    super(name, surname, birthYear, codiceFiscale);
    this.courseYear = courseYear;
    this.studentId = studentId;
  }


  public void greetProfessor(Professor p) {

    System.out.println("Hello Professor " + p);
  }

  public void greetProfessor(Professor p, String nickname) {

    System.out.format("Hello Professor %s , you are such a %s\n", p, nickname);
  }

  @Override
  public String toString() {

    return super.toString() + " " +studentId;
  }

  public String getStudentId() {

    return studentId;
  }

  public void updateCourseYear() {

    courseYear++;
  }

  public int getCourseYear() {

    return courseYear;
  }
}
