package esame;

abstract public class Vendibile {

  private String titolo;
  private Genere genere;
  private int quantita,prezzoCopertina;

  public Vendibile(String titolo, Genere genere, int quantita, int prezzoCopertina) {

    this.titolo = titolo;
    this.genere = genere;
    this.quantita = quantita;
    this.prezzoCopertina = prezzoCopertina;
  }

  public String getTitolo() {

    return titolo;
  }

  public Genere getGenere() {

    return genere;
  }

  public int getQuantita() {

    return quantita;
  }

  public int getPrezzoCopertina() {

    return prezzoCopertina;
  }

  public void increaseQuantita(int incremento) {

    this.quantita += incremento;
  }
}
