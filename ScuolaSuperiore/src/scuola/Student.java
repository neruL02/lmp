package scuola;

import java.time.LocalDate;

public class Student {

  private LocalDate data;

  private String luogoNascita;

  private String nome;

  private String cognome;

  private int annoIscrizione;

  public Student(LocalDate data, String luogoNascita, String nome, String cognome, int annoIscrizione) {

    this.data = data;
    this.luogoNascita = luogoNascita;
    this.nome = nome;
    this.cognome = cognome;
    this.annoIscrizione = annoIscrizione;
  }

  public LocalDate getData() {

    return data;
  }

  public String getLuogoNascita() {

    return luogoNascita;
  }

  public String getNome() {

    return nome;
  }

  public String getCognome() {

    return cognome;
  }

  public int getAnnoIscrizione() {

    return annoIscrizione;
  }
}
