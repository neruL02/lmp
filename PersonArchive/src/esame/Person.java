package esame;


import java.time.LocalDate;

public class Person {
    private String name;
    private String surname;
    private LocalDate birth;
    private String codiceFiscale;

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public LocalDate getBirth() {
        return birth;
    }

    public String getCodiceFiscale() {
        return codiceFiscale;
    }


    public Person(String name, String surname, LocalDate birth, String codiceFiscale) throws PersonCreationException {
        if (birth.getYear() < 1900) {
            throw new PersonCreationException("Cose brutte");
        }
        this.name = name;
        this.surname = surname;
        this.birth = birth;
        this.codiceFiscale = codiceFiscale;
    }
}
