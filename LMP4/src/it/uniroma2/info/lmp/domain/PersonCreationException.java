package it.uniroma2.info.lmp.domain;

public class PersonCreationException extends Exception {

  public PersonCreationException(String message) {

    super(message);
  }
}
