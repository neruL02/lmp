package it.uniroma2.info.lmp.domain;

public class StudentCreationException extends Exception {

  public StudentCreationException(String message) {

    super(message);
  }

  public StudentCreationException(Throwable cause) {

    super(cause);
  }
}
