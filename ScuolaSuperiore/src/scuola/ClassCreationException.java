package scuola;

public class ClassCreationException extends Exception {

  public ClassCreationException(String message) {

    super(message);
  }
}
