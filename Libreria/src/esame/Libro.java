package esame;

public class Libro extends Vendibile {
  private String editore;
 private String[] autori;

  private int numeroPagine,annoPubblicazione,costo;

  public Libro(String titolo, Genere genere, int quantita, int prezzoCopertina, String editore, String[] autori,
               int numeroPagine, int annoPubblicazione) {

    super(titolo, genere, quantita, prezzoCopertina);
    this.editore = editore;
    this.autori = autori;
    this.numeroPagine = numeroPagine;
    this.annoPubblicazione = annoPubblicazione;
    costo = prezzoCopertina;
  }

  public void prezzoSaldo(int costo) throws LibroSaldoException {
    if(costo < getPrezzoCopertina()/2)
      throw new LibroSaldoException("La direttiva del ministero vieta il saldo maggiore del 50% del valore di " +
                                              "copertina.");

    this.costo = costo;
  }

  public String[] getAutori() {

    return autori;
  }
}
