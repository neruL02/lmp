package esame;

public class LibroSaldoException extends Exception {

  public LibroSaldoException(String message) {

    super(message);
  }
}
