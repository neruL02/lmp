package scuola;

import java.util.List;

public interface ClasseI {
  void iscriviStudente(Student studente);
  int numeroStudenti();
  List<StudenteBocciato<Student,Integer>> listaStudentiBocciati();
}
