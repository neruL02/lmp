package it.uniroma2.info.lmp.domain;

import java.util.*;

public class UniversityImplSwitch implements University {

  Map<String, Student> enrolledStudents;
  Set<Course> courses;

  private int progressive = 0;

  private int minCourseYear;

  private int maxCourseYear;

  public UniversityImplSwitch(int minCourseYear, int maxCourseYear) {
      this.minCourseYear = minCourseYear;
      this.maxCourseYear = maxCourseYear;
      enrolledStudents = new HashMap<>();
      courses = new HashSet<>();
  }


  public Student enrollStudent(String name, String surname, int birthYear, String codiceFiscale, Course course,
                               int courseYear) throws StudentCreationException, PersonCreationException {

    if (courseYear < minCourseYear || courseYear > maxCourseYear) {
      throw new StudentCreationException("Course Year: " + courseYear + " is invalid. Accepted values are 1-6.");
    }
    if (!courses.contains(course)){
      throw new StudentCreationException("Course "+course+" not provided.");
    }
    String studentId = course.getCode() + progressive++;
    Student st = new StudentImpl(name, surname, birthYear, codiceFiscale, studentId, course, courseYear);
    enrolledStudents.put(studentId,st);
    return st;
  }

  public Student enrollStudent(String name, String surname, int birthYear, String codiceFiscale, Course course) throws StudentCreationException, PersonCreationException {

    return this.enrollStudent(name, surname, birthYear, codiceFiscale, course, 1);
  }

  public Student getStudent(String studentId) {

    return enrolledStudents.get(studentId);
  }

  public Student enrollCourseStudent(String name, String surname, int birthYear, String codiceFiscale, Course course,
                               int courseYear) throws StudentCreationException, PersonCreationException {
    if (courseYear < minCourseYear || courseYear > maxCourseYear) {
      throw new StudentCreationException("Course Year: " + courseYear + " is invalid. Accepted values are 1-6.");
    }
    if (!courses.contains(course)){
      throw new StudentCreationException("Course "+course+" not provided.");
    }
    String studentId = course.getCode() + progressive++;
    Student st;
    switch (course) {
      case COMPUTER_SCIENCE: {
        st = new ComputerScienceStudentImpl(name, surname, birthYear, codiceFiscale, studentId, course, courseYear);
      }
      case PSYCHOLOGY: {
        st = new PsychologyStudentImpl(name, surname, birthYear, codiceFiscale, studentId, course, courseYear);
      }
      default:{
        st = new StudentImpl(name, surname, birthYear, codiceFiscale, studentId, course, courseYear);
      }
    }
    enrolledStudents.put(studentId,st);
    return st;
  }
    @Override
  public void addCourse(Course course) {
    courses.add(course);
  }

  public Collection<Student> listStudents() {

    return enrolledStudents.values();
  }
}
