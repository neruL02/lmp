package it.uniroma2.info.lmp.domain;

public class ProfessorImpl extends PersonImpl implements Professor{
   private int level;
    public ProfessorImpl(String name, String surname, int birthYear, String codiceFiscale, int level) throws PersonCreationException{
        super(name, surname, birthYear, codiceFiscale);
        this.level = level;
    }

    public ProfessorImpl(Person p, int level) throws PersonCreationException{
        this(p.getName(),p.getSurname(),p.getBirthYear(),p.getCodiceFiscale(),level);
    }

    public int getLevel() {
        return level;
    }
}
