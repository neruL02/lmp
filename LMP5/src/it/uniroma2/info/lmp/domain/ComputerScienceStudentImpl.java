package it.uniroma2.info.lmp.domain;

public class ComputerScienceStudentImpl extends StudentImpl implements ComputerScienceStudent{

  public ComputerScienceStudentImpl(String name, String surname, int birthYear, String codiceFiscale,
                                    String studentId, Course course, int courseYear) throws StudentCreationException, PersonCreationException {

    super(name, surname, birthYear, codiceFiscale, studentId, course, courseYear);
  }
}
