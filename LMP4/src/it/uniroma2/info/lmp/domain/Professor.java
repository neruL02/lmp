package it.uniroma2.info.lmp.domain;

public interface Professor extends Person{
    int getLevel();
}
