package it.uniroma2.info.lmp.domain;

public class PsychologyStudent extends StudentImpl implements Student {
    public PsychologyStudent(String name, String surname, int birthYear, String codiceFiscale) {
        super(name, surname, birthYear, codiceFiscale, "PSY");
    }
}
