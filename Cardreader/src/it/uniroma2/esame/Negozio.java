package it.uniroma2.esame;

public class Negozio extends AttivitaCommerciale {
  String merceVenduta;

  public Negozio(String sede, int inizioAttivita, String partitaIVA, String merceVenduta) {

    super(sede, inizioAttivita, partitaIVA);
    this.merceVenduta = merceVenduta;
  }

  public String toString() {

    return "Negozio{" +
                   "Ha la merce Venduta='" + merceVenduta + '\'' +
                   ", con partitaIVA='" + partitaIVA + '\'' +
                   ", con sede='" + sede + '\'' +
                   ", in attività dal=" + attivitaDal +
                   '}';
  }
}
