package it.uniroma2.info.lmp.domain;

import java.time.LocalDate;


public class Person {
    private String name, surname, codiceFiscale;
    private int birthYear;

    public Person(String name, String surname, int birthYear, String codiceFiscale) {
        this.name = name;
        this.surname = surname;
        this.birthYear = birthYear;
        this.codiceFiscale = codiceFiscale;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getCodiceFiscale() {
        return codiceFiscale;
    }

    public int getAge() {
        return LocalDate.now().getYear() - birthYear;
    }

    public String toString() {
        return getName() + " " + getSurname() + " " + getAge() + " " + getCodiceFiscale();
    }
}
