package it.uniroma2.info.lmp.domain;

import java.util.Collection;

public interface University {

  Student enrollStudent(String name, String surname, int birthYear, String codiceFiscale, Course course,
                        int courseYear) throws StudentCreationException, PersonCreationException;

  Student enrollStudent(String name, String surname, int birthYear, String codiceFiscale, Course course) throws StudentCreationException, PersonCreationException;

  Student getStudent(String studentId);

  Collection<Student> listStudents();

  void addCourse(Course course);
  default void printEnrolledStudents() {

    for (Student student : listStudents()
    ) {
      System.out.println(student);
    }
  }
}
