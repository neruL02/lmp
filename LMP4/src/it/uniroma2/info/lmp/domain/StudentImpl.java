package it.uniroma2.info.lmp.domain;

public class StudentImpl extends PersonImpl implements Student {

  private static int progressive = 0;

  private String studentId;

  private int courseYear;

  public StudentImpl(String name, String surname, int birthYear, String codiceFiscale, Course course,
                     int courseYear) throws StudentCreationException,PersonCreationException {

    super(name, surname, birthYear, codiceFiscale);
    if (courseYear < 1 || courseYear > 6) {
      throw new StudentCreationException("Course Year: " + courseYear + " is invalid. Accepted values are 1-6.");
    }
    this.courseYear = courseYear;
    studentId = course.getCode() + progressive++;
  }

  public StudentImpl(String name, String surname, int birthYear, String codiceFiscale, Course course) throws StudentCreationException,PersonCreationException{

    this(name, surname, birthYear, codiceFiscale, course, 1);
  }

  public void greetProfessor(Professor p) {

    System.out.println("Hello Professor " + p);
  }

  public void greetProfessor(Professor p, String nickname) {

    System.out.format("Hello Professor %s , you are such a %s\n", p, nickname);
  }

  public String getStudentId() {

    return studentId;
  }

  public void updateCourseYear() {

    courseYear++;
  }

  public int getCourseYear() {

    return courseYear;
  }
}
