package it.uniroma2.esame;

public abstract class Attivita {

  String sede;

  int attivitaDal;

  protected Attivita(String sede, int inizioAttivita) {

    this.sede = sede;
    attivitaDal = inizioAttivita;
  }

  public String getSede() {

    return sede;
  }

  public void setSede(String sede) {

    this.sede = sede;
  }

  @Override
  public String toString() {

    return "Attivita:" +
                   "sede='" + sede + '\'' +
                   ", attivitaDal=" + attivitaDal +
                   '}';
  }

  public int getAttivitaDal() {

    return attivitaDal;
  }
}
