package it.uniroma2.info.lmp.domain;

public enum Course {
  COMPUTER_SCIENCE("Computer Science","CS"),
  PSYCHOLOGY("Psychology","PSY"),
  MATH("Mathematics","MAT");

  private String code;
  private String label;

  Course(String label, String code) {

    this.code = code;
    this.label = label;
  }

  public String getCode() {

    return code;
  }

  public String getLabel() {

    return label;
  }
}
