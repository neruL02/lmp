package esame;

public class BuyException extends Exception{

  public BuyException(String message) {

    super(message);
  }
}
