package esame;

public class PersonCreationException extends Exception{
    public PersonCreationException(String message) {
        super(message);
    }
}
