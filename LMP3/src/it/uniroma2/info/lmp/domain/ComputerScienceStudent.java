package it.uniroma2.info.lmp.domain;

public class ComputerScienceStudent extends StudentImpl implements Student {
    public ComputerScienceStudent(String name, String surname, int birthYear, String codiceFiscale) {
        super(name, surname, birthYear, codiceFiscale, "INF");
    }
}
