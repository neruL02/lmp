package it.uniroma2.info.lmp.domain;

public class Runner {
    public static void main(String[] args) {

        Person marco = new Person("Marco", "Rossi", 1995, "sjdkfjdkewksk39");
        Person giorgio = new Person("Giorgio", "Neri", 1984, "irjfcmasklssier");

        System.out.println("Marco --" + marco.getName() + " " + marco.getSurname() + " " + marco.getCodiceFiscale() + " ha " + marco.getAge() + " anni.");
        System.out.println(giorgio);
    }
}
