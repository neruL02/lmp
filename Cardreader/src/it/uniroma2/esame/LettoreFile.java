package it.uniroma2.esame;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class LettoreFile {

  private Map<String, String> datiLetti;

  public LettoreFile() {

    datiLetti = new HashMap<>();
  }

  public Set<String> leggiFile(File file) throws IOException {
    datiLetti.clear();
    BufferedReader buffer = new BufferedReader(new FileReader(file));
   String line;
    while((line = buffer.readLine() )!= null){
      String[] valori = line.split(":");
      datiLetti.put(valori[0],valori[1]);
    }
    return datiLetti.keySet();
  }

  public String leggiAttributo(String o) {

    return datiLetti.get(o);
  }

}
