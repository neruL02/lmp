package scuola;

import java.time.LocalDate;

public class Runner {

  public static void main(String[] args) {
    Student antonio = new Student(LocalDate.of(2002,3,25),"Roma","Antonio","Pimpo",2018);
    Student manola = new Student(LocalDate.of(2001,12,29),"Aprilia","Manola","Gorgi",2017);
    try {
      Classe terzaA = new Classe("Nerone",3,2018,'A');
      terzaA.iscriviStudente(antonio);
      terzaA.iscriviStudente(manola);
      System.out.println(terzaA.getNomeClasse()+" numero studenti: "+terzaA.numeroStudenti());
      for (StudenteBocciato<Student,Integer> s: terzaA.listaStudentiBocciati()
           ) {
        System.out.println(s);
      }
    } catch (ClassCreationException e) {
      e.printStackTrace();
    }

  }
}
