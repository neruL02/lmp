package esame;

import java.time.LocalDate;
@StatusLavorativo(status="Lavorante")
public class Disoccupato extends Person{
    private String iscrizioneRegistroDisoccupazione;

    public String getIscrizioneRegistroDisoccupazione() {
        return iscrizioneRegistroDisoccupazione;
    }

    public Disoccupato(String name, String surname, LocalDate birth, String codiceFiscale, String inscrizioneRegistroDisoccupazione) throws PersonCreationException {
        super(name, surname, birth, codiceFiscale);
    }
}
