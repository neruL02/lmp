package it.uniroma2.info.lmp;

import it.uniroma2.info.lmp.domain.*;

public class Runner {

  public static void main(String[] args) {

//    Person marco = new PersonImpl("Marco", "Rossi", 1995, "sjdkfjdkewksk39");
//    Person giorgio = new PersonImpl("Giorgio", "Neri", 1984, "irjfcmasklssier");

//    System.out.println("Marco --" + marco.getName() + " " + marco.getSurname() + " " + marco.getCodiceFiscale() + "
//    " +
//                               "ha " + marco.getAge() + " anni.");
//    System.out.println(giorgio);

//    Professor stellato = new ProfessorImpl("Armando", "Stellato", 1975, "COSEBRUTTE92L129", 3);
//    Professor rossi = new ProfessorImpl(marco, 1);
    University torVergata = new UniversityImpl(1, 6);
    torVergata.addCourse(Course.COMPUTER_SCIENCE);
    torVergata.addCourse(Course.PSYCHOLOGY);
    torVergata.addCourse(Course.MATH);
    try {
      Student antonio = torVergata.enrollStudent("Antonio", "Fidaleo", 1995, "FDLNTO95G1593l023",
              Course.COMPUTER_SCIENCE);

      //     Student antonio = new StudentImpl("Antonio","Fidaleo", 1995,"FDLNTO95G1593l023","0101300");

//      antonio.greetProfessor(rossi, "Good Prof!");

      Student brock = torVergata.enrollStudent("Brock", "Peters", 1973, "PTRSKF293L129I", Course.PSYCHOLOGY);
//      System.out.printf("The StudentID of Brock is %s\n", brock.getStudentId());

      Student misty = torVergata.enrollStudent("Misty", "Waters", 1992, "WTRSMSY929l129Z", Course.MATH, 6);
//      System.out.println("The StudentID of Misty is " + misty.getStudentId());

    } catch (StudentCreationException | PersonCreationException e) {
      e.printStackTrace();
    }
    torVergata.printEnrolledStudents();
  }
}
