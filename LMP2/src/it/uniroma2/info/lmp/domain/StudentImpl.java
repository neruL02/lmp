package it.uniroma2.info.lmp.domain;

public class StudentImpl extends PersonImpl implements Student {

    String studentId;

    public StudentImpl(String name, String surname, int birthYear, String codiceFiscale, String matricola) {
        super(name, surname, birthYear, codiceFiscale);
        studentId = matricola;
    }


    public void greetProfessor(Professor p) {
        System.out.println("Hello Professor " + p);
    }

    public void greetProfessor(Professor p, String nickname) {
        System.out.format("Hello Professor %s , you are such a %s\n", p, nickname);
    }

    public String getStudentId() {
        return studentId;
    }
}
