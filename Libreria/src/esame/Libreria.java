package esame;


import java.util.*;

public class Libreria {
  Set<Libro> listaLibri;
  Map<Genere, List<Vendibile>> sezioni;
  List<Vendibile> listaNonDisponibili;

  public Libreria() {
    sezioni = new HashMap<>();
    listaNonDisponibili = new ArrayList<>();
    listaLibri = new HashSet<>();
  }
  public void aggiungiOggetto(Vendibile vendibile) {

    List<Vendibile> listaLibri = sezioni.get(vendibile.getGenere());
    if (listaLibri == null) {
      listaLibri = new ArrayList<>();
      sezioni.put(vendibile.getGenere(), listaLibri);
    }
    listaLibri.add(vendibile);
    if (vendibile.getClass().equals(Libro.class))
      this.listaLibri.add((Libro) vendibile);
  }
  public List<Libro> libriAutore(String autore){
    List<Libro> libri = new ArrayList<>();
    for (Libro libro: listaLibri
         ) {
      for (String aut : libro.getAutori()) {
        if (aut.equals(autore))
          libri.add(libro);
      }
    }
    return libri;
  }

  void compra(Vendibile vendibile) throws BuyException {
    if (vendibile.getQuantita() == 0)
      throw new BuyException("Il vendibile non è disponibile. Informare il proprietario.");
    vendibile.increaseQuantita(-1);
    if (vendibile.getQuantita() == 0){
      listaNonDisponibili.add(vendibile);
    }
  }

  public List<Vendibile> getListaNonDisponibili() {

    return listaNonDisponibili;
  }

  public List<Vendibile> catalogo() {
    List<Vendibile> lista = new ArrayList<>();
    for (Genere genere: Genere.values()) {
      List<Vendibile> list = sezioni.get(genere);
      lista.addAll(list);
    }
    return lista;
  }
}
