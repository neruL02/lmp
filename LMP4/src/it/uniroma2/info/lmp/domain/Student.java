package it.uniroma2.info.lmp.domain;

public interface Student extends Person {
    void greetProfessor(Professor p);
    void greetProfessor(Professor p, String nickname);
    String getStudentId();
}
