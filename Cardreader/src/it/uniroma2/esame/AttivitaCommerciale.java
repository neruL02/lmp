package it.uniroma2.esame;

public abstract class AttivitaCommerciale extends Attivita {
  String partitaIVA;

  protected AttivitaCommerciale(String sede, int inizioAttivita, String partitaIVA) {

    super(sede, inizioAttivita);
    this.partitaIVA = partitaIVA;
  }

  public String getPartitaIVA() {

    return partitaIVA;
  }

  public void setPartitaIVA(String partitaIVA) {

    this.partitaIVA = partitaIVA;
  }
}
