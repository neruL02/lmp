package it.uniroma2.info.lmp.domain;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

public class UniversityImpl implements University {

  Map<String, Student> enrolledStudents;
  Set<Course> courses;
  Map<Course,Class<? extends Student>> studentClasses;

  private int progressive = 0;

  private int minCourseYear;

  private int maxCourseYear;

  public UniversityImpl(int minCourseYear,int maxCourseYear) {
      this.minCourseYear = minCourseYear;
      this.maxCourseYear = maxCourseYear;
      enrolledStudents = new HashMap<>();
      courses = new HashSet<>();
      studentClasses = new HashMap<>();
  }

  public Student getStudent(String studentId) {

    return enrolledStudents.get(studentId);
  }

  public Student enrollStudent(String name, String surname, int birthYear, String codiceFiscale, Course course,
                               int courseYear) throws StudentCreationException, PersonCreationException {
    if (courseYear < minCourseYear || courseYear > maxCourseYear) {
      throw new StudentCreationException("Course Year: " + courseYear + " is invalid. Accepted values are 1-6.");
    }
    if (!courses.contains(course)){
      throw new StudentCreationException("Course "+course+" not provided.");
    }
    String studentId = course.getCode() + progressive++;
    Class<? extends Student> cst;
    if (studentClasses.containsKey(course)){
      cst = studentClasses.get(course);
    }
    else cst = StudentImpl.class;
    try {
      Constructor<?> cstr = cst.getDeclaredConstructor(String.class, String.class, int.class, String.class, String.class, Course.class, int.class);
      Student st = (Student) cstr.newInstance(name, surname, birthYear, codiceFiscale, studentId, course,
              courseYear);
      enrolledStudents.put(studentId,st);
      return st;
    }
    catch (Exception e){
      throw new StudentCreationException(e);
    }

  }

  public Student enrollStudent(String name, String surname, int birthYear, String codiceFiscale, Course course) throws StudentCreationException, PersonCreationException {

    return this.enrollStudent(name, surname, birthYear, codiceFiscale, course, 1);
  }

    @Override
  public void addCourse(Course course) {
    courses.add(course);
  }

  public Collection<Student> listStudents() {

    return enrolledStudents.values();
  }
}
