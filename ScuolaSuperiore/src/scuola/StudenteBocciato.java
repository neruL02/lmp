package scuola;

public class StudenteBocciato<T extends Student,V extends Integer> {
 private T studente;
 private V anniRipetente;

  public StudenteBocciato(T studente, V anniRipetente) {

    this.studente = studente;
    this.anniRipetente = anniRipetente;
  }

  public T getStudente() {

    return studente;
  }

  public V getAnniRipetente() {

    return anniRipetente;
  }


  public String toString() {

    return "Nome studente: "+ studente.getCognome()+" "+studente.getNome()+". Ripetente da "+anniRipetente+" anni.";
  }
}
