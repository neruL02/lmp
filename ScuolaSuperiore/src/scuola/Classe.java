package scuola;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Classe extends ScuolaSuperiore implements ClasseI {

  private int annoCorso;

  private int annoStudenti;

  private char sezione;

  private String nomeClasse;

  private Set<Student> registroClasse;

  private List<StudenteBocciato<Student, Integer>> listaBocciati;

  public Classe(String nomeScuola, int annoCorso, int annoStudenti, char sezione) throws ClassCreationException {

    super(nomeScuola);
    if (annoCorso < 1 || annoCorso > 5)
      throw new ClassCreationException("Anno di corso non valido. I valori consentiti sono compresi tra 1 e 5.");

    this.annoCorso = annoCorso;
    this.annoStudenti = annoStudenti;
    this.sezione = sezione;
    this.nomeClasse = String.valueOf(annoCorso) + sezione;
    registroClasse = new HashSet<>();
    listaBocciati = new ArrayList<>();
  }

  public String getNomeClasse() {

    return nomeClasse;
  }

  public void iscriviStudente(Student studente) {

    registroClasse.add(studente);
    int anno = studente.getAnnoIscrizione();
    if (anno < annoStudenti) {
      listaBocciati.add(new StudenteBocciato<>(studente, (annoStudenti - anno)));
    }
  }

  public int numeroStudenti() {

    return registroClasse.size();
  }

  public List<StudenteBocciato<Student, Integer>> listaStudentiBocciati() {

    return listaBocciati;
  }
}
