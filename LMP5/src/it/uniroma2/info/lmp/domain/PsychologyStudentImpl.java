package it.uniroma2.info.lmp.domain;

public class PsychologyStudentImpl extends StudentImpl implements PsychologyStudent{

  public PsychologyStudentImpl(String name, String surname, int birthYear, String codiceFiscale, String studentId,
                               Course course, int courseYear) throws StudentCreationException, PersonCreationException {

    super(name, surname, birthYear, codiceFiscale, studentId, course, courseYear);
  }
}
