package it.uniroma2.info.lmp.domain;

public abstract class StudentImpl extends PersonImpl implements Student {

  String studentId;
  static int progressive = 0;

  protected StudentImpl(String name, String surname, int birthYear, String codiceFiscale, String code) {
    super(name, surname, birthYear, codiceFiscale);
    studentId = code + progressive++;
  }


  public void greetProfessor(Professor p) {
    System.out.println("Hello Professor " + p);
  }

  public void greetProfessor(Professor p, String nickname) {
    System.out.format("Hello Professor %s , you are such a %s\n", p, nickname);
  }

  public String getStudentId() {
    return studentId;
  }
}
