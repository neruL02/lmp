package esame;

import java.time.LocalDate;
@StatusLavorativo(status = "Lavorante")

public class LiberoProfessionista extends Person {
    private String professione;
    private String partitaIVA;

    public String getProfessione() {
        return professione;
    }

    public String getPartitaIVA() {
        return partitaIVA;
    }

    public LiberoProfessionista(String name, String surname, LocalDate birth, String codiceFiscale, String professione, String partitaIVA) throws PersonCreationException {
        super(name, surname, birth, codiceFiscale);
        this.professione = professione;
        this.partitaIVA = partitaIVA;
    }
}
