package it.uniroma2.info.lmp;

import it.uniroma2.info.lmp.domain.*;

public class Runner {
    public static void main(String[] args) {

        Person marco = new PersonImpl("Marco", "Rossi", 1995, "sjdkfjdkewksk39");
        Person giorgio = new PersonImpl("Giorgio", "Neri", 1984, "irjfcmasklssier");

        System.out.println("Marco --" + marco.getName() + " " + marco.getSurname() + " " + marco.getCodiceFiscale() + " ha " + marco.getAge() + " anni.");
        System.out.println(giorgio);

        Professor stellato = new ProfessorImpl("Armando","Stellato",1975,"COSEBRUTTE92L129",3);
        Professor rossi = new ProfessorImpl(marco,1);

        Student antonio = new StudentImpl("Antonio","Fidaleo", 1995,"FDLNTO95G1593l023","0101300");
        Student gianluca = new AngryStudent("Gianluca", "Neri", 1996,"StringaACAso","0298743");

        antonio.greetProfessor(rossi,"Good Prof!");
        gianluca.greetProfessor(stellato);
    }
}
