package it.uniroma2.esame;

import java.io.File;
import java.io.IOException;

public class CardReader {
  LettoreFile lf;

  public CardReader() {
  lf = new LettoreFile();
  }

  public Attivita creaAttivita(String fileName) throws IOException {
    lf.leggiFile(new File(fileName));
    String fileType = lf.leggiAttributo("filetype");
    switch (fileType){
      case "negozio": {
        String pIVA = lf.leggiAttributo("partita_IVA");
        String sede = lf.leggiAttributo("sede");
        String merceVenduta = lf.leggiAttributo("merce_venduta");
        int attivitaDal = Integer.parseInt(lf.leggiAttributo("in_attività_dal"));
        return new Negozio(sede,attivitaDal,pIVA,merceVenduta);
      }
      default: return null;
    }
  }
}
