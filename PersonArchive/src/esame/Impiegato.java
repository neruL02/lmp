package esame;

import java.time.LocalDate;

@StatusLavorativo(status = "Lavorante")
public class Impiegato extends Person {
    private String matricola;
    private int livello;
    private String mansione;

    public String getMatricola() {
        return matricola;
    }

    public int getLivello() {
        return livello;
    }

    public String getMansione() {
        return mansione;
    }

    public Impiegato(String name, String surname, LocalDate birth, String codiceFiscale, String matricola, int livello, String mansione) throws PersonCreationException {
        super(name, surname, birth, codiceFiscale);
        this.matricola = matricola;
        this.livello = livello;
        this.mansione = mansione;
    }
}
