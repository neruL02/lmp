package it.uniroma2.info.lmp.domain;

import java.time.LocalDate;


public class PersonImpl implements Person {
    private String name, surname, codiceFiscale;
    private int birthYear;

    public PersonImpl(String name, String surname, int birthYear, String codiceFiscale) throws PersonCreationException{
        this.name = name;
        this.surname = surname;
        if (birthYear>=3000) throw new PersonCreationException("Invalid Birth Year:"+birthYear+". It must be less " +
                                                                       "than 3000");
        this.birthYear = birthYear;
        this.codiceFiscale = codiceFiscale;
    }

    @Override
    public int getBirthYear() {
        return birthYear;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getSurname() {
        return surname;
    }

    @Override
    public String getCodiceFiscale() {
        return codiceFiscale;
    }

    @Override
    public int getAge() {
        return LocalDate.now().getYear() - birthYear;
    }

    @Override
    public String toString() {
        return getName() + " " + getSurname() + " " + getAge() + " " + getCodiceFiscale();
    }
}
