package it.uniroma2.esame;

public class Associazione extends Attivita {
  Scopo scopo;

  public Associazione(String sede, int inizioAttivita, Scopo scopo) {

    super(sede, inizioAttivita);
    this.scopo = scopo;
  }
}
