package it.uniroma2.info.lmp;

import it.uniroma2.info.lmp.domain.*;

public class Runner {
    public static void main(String[] args) {

        Person marco = new PersonImpl("Marco", "Rossi", 1995, "sjdkfjdkewksk39");
        Person giorgio = new PersonImpl("Giorgio", "Neri", 1984, "irjfcmasklssier");

        System.out.println("Marco --" + marco.getName() + " " + marco.getSurname() + " " + marco.getCodiceFiscale() + " ha " + marco.getAge() + " anni.");
        System.out.println(giorgio);

        Professor stellato = new ProfessorImpl("Armando","Stellato",1975,"COSEBRUTTE92L129",3);
        Professor rossi = new ProfessorImpl(marco,1);

        Student antonio = new ComputerScienceStudent("Antonio","Fidaleo", 1995,"FDLNTO95G1593l023");

   //     Student antonio = new StudentImpl("Antonio","Fidaleo", 1995,"FDLNTO95G1593l023","0101300");

        antonio.greetProfessor(rossi,"Good Prof!");

        Student brock = new ComputerScienceStudent("Brock", "Peters", 1973,"PTRSKF293L129I");
        System.out.printf("The StudentID of Brock is %s\n",brock.getStudentId());

        Student misty = new PsychologyStudent("Misty","Waters", 1992,"WTRSMSY929l129Z");
        System.out.println("The StudentID of Misty is "+misty.getStudentId());
    }
}
